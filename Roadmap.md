Here is a checklist of way forward

* [x]  Spawn courier
* [x]  Talk about F1 key to center your hero
* [x]  If levels gained are not spent then game would pause and request user to spend it on learning abilities
* [x]  At level 3 an enemy hero of easy level difficulty is spawned
* [x]  Low health and mana would now ask user to consume tango / clarity
* [x]  Talk about being able to combine items
* [x]  Talk about courier
* [x]  After acquiring required gold buy wraith band and use courier to get it
* [x]  Send courier to secret shop to complete manta and talk about secret shops
* [x]  Pausing a game with F9


Advanced Tutorial later
* [ ]  Talent tree
* [ ]  Talk about barracks
* [ ]  Spawn a dd rune at 4 minutes
* [ ]  Talk about runes
* [ ]  Let luna get dd rune
* [ ]  Adding yasha to stash and buying each component
* [ ]  In case of death, TP to mid tower - bind TP to space key as by default its not linked to anything


