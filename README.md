Trying to create a good tutorial that covers most of the basics and helps people with steps forward.

# Running latest code

Clone or download the code and copy it to the following directory inside steam library
 
`dota 2 beta/game/dota_addons/`

After adding it, launch dota 2 and then in console type the below command

`dota_launch_custom_game dota-tutorial dota`

### Note:
At times tutorial stops loading giving a dialog of text. To fix this

1. Launch mechanics tutorial from learn tab
2. Close it
3. Open console and enter the command above
