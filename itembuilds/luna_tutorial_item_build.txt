"itembuilds"
{
	"author"		"Valve"
	"hero"			"npc_dota_hero_luna"
	"Title"			"#DOTA_Item_Build_Tutorial_Items_For_Luna"
	
	"Items"
	{		
		"#DOTA_Item_Build_Starting_Items"
		{
			"item"		"item_tango"
			"item"		"item_flask"
			"item"		"item_clarity"
			"item"		"item_circlet"
			"item"		"item_slippers"
		}

		"#Laning_Items"
        {
            "item"		"item_wraith_band"
            "item"		"item_power_treads"
            "item"		"item_yasha"
        }
        "#Midgame_Items"
        {
            "item"		"item_manta"
        }
	}
}